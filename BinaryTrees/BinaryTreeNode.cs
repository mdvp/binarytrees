﻿namespace BinaryTrees
{
    internal class BinaryTreeNode<T> : Node<T>
    {
        public BinaryTreeNode() : base() { }
        public BinaryTreeNode(T value) : base(value, null) { }
        public BinaryTreeNode(T value, BinaryTreeNode<T> left, BinaryTreeNode<T> right)
        {
            base.Value = value;
            base.Descendants = new NodeList<T> { left, right };
        }

        public BinaryTreeNode<T> Left
        {
            get
            {
                if (base.Descendants == null)
                    return null;
                else
                    return (BinaryTreeNode<T>)base.Descendants[0];
            }
            set
            {
                if (base.Descendants == null)
                    base.Descendants = new NodeList<T>(2);

                base.Descendants[0] = value;
            }
        }

        public BinaryTreeNode<T> Right
        {
            get
            {
                if (base.Descendants == null)
                    return null;
                else
                    return (BinaryTreeNode<T>)base.Descendants[1];
            }
            set
            {
                if (base.Descendants == null)
                    base.Descendants = new NodeList<T>(2);

                base.Descendants[1] = value;
            }
        }

        public bool IsLeaf
        {
            get
            {
                return Left == null && Right == null;
            }
        }
    }
}
