﻿namespace BinaryTrees
{
    internal class Node<T>
    {
        public T Value { get; set; }
        protected NodeList<T> Descendants { get; set; }

        public Node() { }
        public Node(T value) : this(value, null) { }
        public Node(T value, NodeList<T> descendants)
        {
            this.Value = value;
            this.Descendants = descendants;
        }
    }
}
