﻿using System.Collections.ObjectModel;

namespace BinaryTrees
{
    internal class NodeList<T> : Collection<Node<T>>
    {
        public NodeList(int initialSize)
        {
            for (int i = 0; i < initialSize; i++)
                base.Items.Add(default(Node<T>));
        }

        //public Node<T> FindByValue(T value)
        //{
        //    foreach (var node in Items)
        //    {
        //        if (node.Value.Equals(value))
        //            return node;
        //    }

        //    return null;
        //}

        public NodeList() : base() { }
    }
}
