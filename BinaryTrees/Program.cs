﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTrees
{
    class Program
    {
        static void Main(string[] args)
        {
            var tree = new BinaryTree<int>();

            //tree.Root = new BinaryTreeNode<int>(1);
            //tree.Root.Left = new BinaryTreeNode<int>(2);
            //tree.Root.Right = new BinaryTreeNode<int>(3);

            //tree.Root.Left.Left = new BinaryTreeNode<int>(4);
            //tree.Root.Right.Right = new BinaryTreeNode<int>(5);

            //tree.Root.Left.Left.Right = new BinaryTreeNode<int>(6);
            //tree.Root.Right.Right.Right = new BinaryTreeNode<int>(7);

            //tree.Root.Right.Right.Right.Right = new BinaryTreeNode<int>(8);

            //tree.PreorderTraversal(tree.Root);


            for (int i = 1; i <= 8; i++)
            {
                tree.Add(i);
            }

            tree.Remove(5);

            Console.WriteLine(tree.Count);
        }
    }
}
